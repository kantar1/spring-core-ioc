package com.kantar.spring.core.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class Customer {
	
	int id;
	String name;
	String email;
	String contactNo;
	
		
	Order order;
	
	
	  public Customer() { System.out.println("Customer() is called..."); }
	 
	
    @Autowired
	public Customer(Order order) {
		System.out.println("Customer(Order) is called.");
		this.order = order;
	}
	
    
	public Customer(int id, String name, String email, String contactNo, Order order) {
		System.out.println("Customer(int,String,String,String) is called...");
		this.id = id;
		this.name = name;
		this.email = email;
		this.contactNo = contactNo;
		this.order = order;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getContactNo() {
		return contactNo;
	}



	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}



	public Order getOrder() {
		return order;
	}



	public void setOrder(Order order) {
		//System.out.println("order value is set");
		this.order = order;
	}



	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name + ", email=" + email + ", contactNo=" + contactNo + ", order="
				+ order + "]";
	}
	
	public void init() {
		System.out.println("init() is called");
	}
	
	public void destroy() {
		System.out.println("destroy() is called.");
	}

	
	
	

	
}
