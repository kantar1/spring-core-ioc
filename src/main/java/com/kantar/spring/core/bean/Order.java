package com.kantar.spring.core.bean;

import org.springframework.stereotype.Component;

@Component
public class Order {
	
	int id;
	double value;
	
	public Order() {
		System.out.println("Order() is called.");
	}

	public Order(int id, double value) {
		System.out.println("Order(int,double) is called.");
		this.id = id;
		this.value = value;
	}
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", value=" + value + "]";
	}
	
	

}
