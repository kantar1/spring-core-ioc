package com.kantar.spring.core.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.kantar.spring.core.bean")
public class Config {
	
	//Factory Methods
	/*
	 * @Bean("customer1") public Customer getCustomer1() { return new
	 * Customer(111,"Steve","steve@gmail.com","+91-879879", getOrder1()); }
	 * 
	 * @Bean("customer2") public Customer getCustomer2() { return new
	 * Customer(222,"Peter","peter@gmail.com","+91-979879", getOrder2()); }
	 */
	
	
	/*
	 * @Bean("order1") public Order getOrder1() { return new Order(111, 4353.34); }
	 * 
	 * @Bean("order2") public Order getOrder2() { return new Order(222, 5353.34); }
	 */
	
	

}
