package com.kantar.spring.core.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.kantar.spring.core.bean.Customer;
import com.kantar.spring.core.bean.Order;
import com.kantar.spring.core.config.Config;

public class Main {

	public static void main(String[] args) {

		//ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(Config.class);

		Customer customer = ctx.getBean(Customer.class);
		System.out.println(customer);
		
		Customer customer2 = ctx.getBean(Customer.class);
		System.out.println(customer2);
		System.out.println(customer == customer2);
		
		
		/*
		Order order1 = ctx.getBean(Order.class);
		order1.setId(111);
		order1.setValue(2423423.23);
		
		customer.setId(111);
		customer.setName("Steve");
		customer.setEmail("steve@gmail.com");
		customer.setContactNo("+91-98798723");
		customer.setOrder(order1);
		
		System.out.println(customer);
		/*
		 * Customer customer = ctx.getBean("customer1", Customer.class);
		 * System.out.println(customer);
		 * 
		 * Customer customer2 = ctx.getBean("customer2", Customer.class);
		 * System.out.println(customer2); Order order2 = ctx.getBean("order2",
		 * Order.class); System.out.println(order2);
		 */
		
		((ConfigurableApplicationContext)ctx).close();

	}

}
